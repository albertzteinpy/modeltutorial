# -*- encoding: utf-8 -*-
from django.db import models

# Create your models here.

class Genre(models.Model):
    class Meta:
        verbose_name = 'Genre'
    genrename = models.CharField(u'',max_length=200)

    def __unicode__(self):
        return u'%s'%self.genrename


class Artist(models.Model):

    class Meta:
        verbose_name="Artist"

    artistname = models.CharField(u'Name',max_length=250)
    artistgenre = models.ManyToManyField(Genre)
    def __unicode__(self):
        return u'%s'%self.artistname


class Disc(models.Model):
    disctitle = models.CharField(u'Title',max_length=200)
    theartist = models.ForeignKey(Artist)
    tags = models.TextField(blank=True,null=True)
    def __unicode__(self):
        return u'%s'%self.disctitle



class Musician(models.Model):
    name = models.CharField(u'Name',max_length=200)
    lastname = models.CharField(u'LastName',max_length=200)
    phone = models.CharField(u'Phone',max_length=200)
    email = models.EmailField(u'Email')
    birthdate = models.DateField(u'Birth')
    address = models.CharField(u'adr',max_length=100)
    def __unicode__(self):
        return u'%s'%self.name


