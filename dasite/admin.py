from django.contrib import admin
from dasite.models import *
from django.db.models import get_app, get_models

#class GenreAdmin(admin.ModelAdmin):
#    model = Genre

#class ArtistAdmin(admin.ModelAdmin):
#    model = Artist



#admin.site.register(Genre,GenreAdmin)
#admin.site.register(modeling[0],modeling[1])
# Register your models here.

app = get_app('dasite')
models = get_models(app)

for m in models:
    classname = u'%sAdmin'%m._meta.verbose_name
    model = m

    SuperClass = type(str(classname),(admin.ModelAdmin,),{"model":model})
    admin.site.register(model,SuperClass)


