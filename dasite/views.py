# -*- encoding: utf-8 -*-
from django.shortcuts import render
from dasite.models import * 
from django.http import HttpResponse
import simplejson

def index(request):
    response_raw = {}
    json_response = simplejson.dumps(response_raw)
    return HttpResponse(json_response,content_type='application/json; charset=utf-8')


def genre(request):
    genresearch = request.GET.get('name',None)
    response_raw = []
    if genresearch:
        gr = Genre.objects.filter(genrename=genresearch).values('genrename')
        gr = Genre.objects.filter(genrename__icontains=genresearch).values('genrename')
    else:
        gr = Genre.objects.all().values('genrename')
    
    for g in gr:
        response_raw.append(g)
    response_raw.append({'query':str(gr.query)})
    json_response = simplejson.dumps(response_raw)
    return HttpResponse([json_response],'application/json; charset=utf-8')


def artist(request):
    args = []
    kwargs = {}
    for rq in request.GET:
        kwargs[rq]=request.GET[rq]
    
    response_raw = []
    artists = Artist.objects.filter(*args,**kwargs).values()
    response_raw.append({'query':str(artists.query)}) 
    for a in artists:
        response_raw.append(a)
    
    json_response = simplejson.dumps(response_raw)
    return HttpResponse(json_response)
    

