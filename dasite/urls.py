from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('dasite.views',
    url(r'^$','index',name='index'),
    url(r'^genre$','genre',name='genre'),
    url(r'^artist$','artist',name='artist'),

)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
